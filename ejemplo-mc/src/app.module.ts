import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { EjemploService } from 'servicio/ejemplo.service';

@Module({
  imports: [],
  controllers: [AppController],
  providers: [
    AppService,
    EjemploService
  ],
})
export class AppModule {}
