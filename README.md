# Servicios en nestjs

Aquí se muestra como crear un servicio para nestjs es decir un servicio REST.

## Creando un servicio

Creamos una carpeta servicio dentro de la carpeta src, y dentro de servicio creamos un archivo .ts llamado ejemplo.service.ts de la siguiente manera.

![](imagenes/directorio-servicio.png)

Dentro de este archivo insertamos el siguiente código
``` TypeScript
@Injectable()
export class EjemploService{

}
```

es importante agregar el decorador Injectable (que se importa de @nestjs/common) debido a que luego se lo debe agregar a algún módulo en la parte de providers. (Esta parte es similar a Angular)

``` TypeScript
@Injectable()
export class EjemploService{

    sumar(numero1: number,numero2: number):number{
        return (numero1+numero2)
    }

}
```
El método sumar devolverá la suma de dos numeros cuando se lo utilice.

## Implementando el servicio
Lo primero que debemos hacer es añadir el servicio al módulo donde lo vayamos a utilizar, en este caso es el módulo principal. Quedará de esta forma.
![](imagenes/appmodule.png)

Luego importamos el servicio en el controllador en este caso:
![](imagenes/controller1.png)

Modificamos el método GET por el siguiente código:
``` TypeScript
  @Get('respuesta')
  funcionReaccionarGet(
    @Query('numero1') numero1,
    @Query('numero2') numero2
  ) {
    return this.ejemploService
      .sumar(parseInt(numero1), parseInt(numero2));
  }
```

Y lo probamos, reiniciamos el servidor con el comando
```
npm run start
```
observamos que los métodos GET y POST estén mapeados.
![](imagenes/serveron.png)

Y finalmente abrimos el navegador en localhost:3000

![](imagenes/test.png)

y eso ha sido todo. Hemos aprendido como crear un servicio para nestjs.

<a href="https://twitter.com/atpsito" target="_blank"><img alt="Sígueme en Twitter" height="35" width="35" src="https://3.bp.blogspot.com/-E4jytrbmLbY/XCrI2Xd_hUI/AAAAAAAAIAo/qXt-bJg1UpMZmTjCJymxWEOGXWEQ2mv3ACLcBGAs/s1600/twitter.png" title="Sígueme en Twitter"/> @atpsito </a><br>
<a href="https://www.linkedin.com/in/alexander-tigselema-ba1443124/" target="_blank"><img alt="Sígueme en LinkedIn" height="35" width="35" src="https://4.bp.blogspot.com/-0KtSvK3BydE/XCrIzgI3RqI/AAAAAAAAH_w/n_rr5DS92uk9EWEegcxeqAcSkV36OWEOgCLcBGAs/s1600/linkedin.png" title="Sígueme en LinkedIn"/> Alexander Tigselema</a><br>
<a href="https://www.instagram.com/atpsito" target="_blank"><img alt="Sígueme en Instagram" height="35" width="35" src="https://4.bp.blogspot.com/-Ilxti1UuUuI/XCrIy6hBAcI/AAAAAAAAH_k/QV5KbuB9p3QB064J08W2v-YRiuslTZnLgCLcBGAs/s1600/instagram.png" title="Sígueme en Instagram"/> atpsito</a><br>